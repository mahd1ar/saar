module.exports = {
  transpileDependencies: ["vuetify"],

  pwa: {
    themeColor: "#2E4867"
  }
};

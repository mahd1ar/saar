/* eslint-disable */

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
require('dotenv').config();

function sleep(time) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, time);
  });
}

app.use(cors());
app.use(async (req, res, next) => {
  await sleep(3000);
  next();
});
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  extended: true,
}));

const port = process.env.SERVERPORT || 3030;
const maxSessionTimeout = process.env.MAXSESSIONTIMEOUT || 60 * 60 * 2; // in secounds

console.log(maxSessionTimeout);

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.post('/api/login', (req, res) => {
  console.log(`token = ${req.headers.authorization}`);
  if (req.body.username === 'admin' && req.body.password === 'admin') {
    res.json({
      token: 'Yjg0MTkxNjAxNzBhNDFhZTAxYWJhYjEzYTNiODg3ZGYgIC0K',
      fullName: 'mahdiyar anari',
      expiresAt: Date.now() + maxSessionTimeout * 1000,
    });
  } else {
    res.status(401).send('somthign');//.json({ msg: 'not permitted' });
  }
});

app.post('/api/test', (req, res) => {
  console.log('testEndpoint');
  console.log(req.body);
  console.log(`token = ${req.headers.authorization}`);
  if (req.headers.authorization !== '') res.json({ ok: 1 });
  else res.status(403).send({ msg: 'tokenexpires' });
});

const ddata = [
  {
    title: 'test API',
    hash: 'NIELUNIUNWIUFNPIUNWPVBUPUVBWPUBVPIPPFWIUBPFBOEBOYBFOBEW',
    datetime: new Date().toLocaleString(),
    user: 'linus torvalds',
  },
  {
    title: 'test API',
    hash: 'NIELUNIUNWIUFNPIUNWPVBUPUVBWPUBVPIPPFWIUBPFBOEBOYBFOBEW',
    datetime: new Date().toLocaleString(),
    user: 'ken thompson',
  },
  {
    title: 'test API',
    hash: 'NIELUNIUNWIUFNPIUNWPVBUPUVBWPUBVPIPPFWIUBPFBOEBOYBFOBEW',
    datetime: new Date().toLocaleString(),
    user: 'dennis Ritchie',
  },
];

app.get('/api/datatable', (req, res) => {
  ddata.forEach((element, index) => {
    element.id = `${index + 1}`;
  });
  res.json(ddata);
});

app.post('/api/register', (req, res) => {
  const mydata = req.body;

  const date = new Date(mydata.datetime);
  mydata.datetime = date.toLocaleString();
  ddata.push(mydata);
  res.json({ 1: 1 });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

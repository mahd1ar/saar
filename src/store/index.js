import Vue from "vue";
import Vuex from "vuex";
import cloneDeep from "lodash.clonedeep";

import { clearStorage, fillStrorage, isUserLoggedIn } from "../helpers/helper";

Vue.use(Vuex);

// const expires = localStorage.getItem('expiresAt');
const userName = localStorage.getItem("username") || "";

const isLoggedIn = isUserLoggedIn();
// expires ? expires > Date.now() : false;

export default new Vuex.Store({
  state: {
    // login parameters
    loading: false,
    isLoggedIn,
    userName,
    // profile
    profile: {},
    // settings
    lang: "fa",
    darkmode: "off",
    // global Msgs
    snackbar: false,
    snackbarMsg: "",
    snackbarState: "green"
  },
  mutations: {
    setProfile(state, payload) {
      state.profile = cloneDeep(payload);
    },
    changelang(state, lang) {
      if (lang === "en") {
        state.lang = "en";
      } else {
        state.lang = "fa";
      }
    },
    waiting(state, payload = true) {
      state.loading = payload;
    },
    logUserIn(state, params) {
      state.userName = params.username;
      state.isLoggedIn = true;
      fillStrorage(params);
    },
    logUserOut(state) {
      state.userName = "";
      state.isLoggedIn = false;
      clearStorage();
    },
    snackbar(state, payload) {
      if (payload) {
        state.snackbar = true;
        state.snackbarMsg = payload.msg;
        state.snackbarState = payload.state === "success" ? "green" : "pink";
      } else state.snackbar = false;
    }
  },
  actions: {},
  modules: {}
});

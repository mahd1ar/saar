const _interopRequireDefault = require("/home/mahdiyar/Documents/proj/saar/node_modules/@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/es.array.some");

require("core-js/modules/es.function.name");

require("core-js/modules/es.object.to-string");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

const _interopRequireWildcard2 = _interopRequireDefault(
  require("/home/mahdiyar/Documents/proj/saar/node_modules/@babel/runtime/helpers/esm/interopRequireWildcard")
);

const _vue = _interopRequireDefault(require("vue"));

const _vueRouter = _interopRequireDefault(require("vue-router"));

const _Home = _interopRequireDefault(require("../views/Home.vue"));

const _helper = require("../helpers/helper");

const _store = _interopRequireDefault(require("../store"));

/* eslint-disable */
// import Register from '../'
_vue.default.use(_vueRouter.default);

var routes = [{
  path: '/',
  name: 'Home',
  component: _Home.default,
  meta: {
    layout: 'deflayout',
    requiresAuth: true
  }
}, {
  path: '/register',
  name: 'Register',
  component: function component() {
    return Promise.resolve().then(function () {
      return (0, _interopRequireWildcard2.default)(require('../views/Register.vue'));
    });
  },
  meta: {
    layout: 'deflayout',
    requiresAuth: true
  }
}, {
  path: '/login',
  name: 'Login',
  component: function component() {
    return Promise.resolve().then(function () {
      return (0, _interopRequireWildcard2.default)(require('../views/Login.vue'));
    });
  },
  meta: {
    layout: 'nolayout'
  }
}, {
  path: '/profile',
  name: 'Profile',
  meta: {
    layout: 'deflayout',
    requiresAuth: true
  },
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  component: function component() {
    return Promise.resolve().then(function () {
      return (0, _interopRequireWildcard2.default)(require('../views/Profile.vue'));
    });
  }
}, {
  path: '*',
  name: 'NotFound',
  meta: {
    layout: 'nolayout',
    requiresAuth: false
  },
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  component: function component() {
    return Promise.resolve().then(function () {
      return (0, _interopRequireWildcard2.default)(require('../views/404.vue'));
    });
  }
}];
var router = new _vueRouter.default({
  routes: routes
});
router.beforeEach(function (to, from, next) {
  var isLoggedIn = (0, _helper.isUserLoggedIn)();

  if (isLoggedIn === false) {
    _store.default.commit('logUserOut');
  }

  var configuration = Object.assign({}, {
    path: to.path,
    params: {
      nextUrl: to.fullPath
    },
    query: {
      lang: from.query.lang || 'fa'
    }
  });

  if (to.name === 'Login' && isLoggedIn) {
    next(Object.assign({}, configuration, {
      path: '/'
    }));
  }

  if (to.matched.some(function (record) {
    return record.meta.requiresAuth;
  })) {
    if (!isLoggedIn) {
      next(Object.assign({}, configuration, {
        path: '/login'
      }));
    } else {
      to.query.lang ? next() : next(configuration);
    }
  } else {
    to.query.lang ? next() : next(configuration);
  }
});
var _default = router;
exports.default = _default;
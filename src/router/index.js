/* eslint-disable */
import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import { isUserLoggedIn } from '../helpers/helper';
import store from '../store';

// import Register from '../'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      layout: 'deflayout',
      requiresAuth: true,
    },
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import(/* webpackChunkName: "register" */ '../views/Register.vue'),
    meta: {
      layout: 'deflayout',
      requiresAuth: true,
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue'),
    meta: {
      layout: 'nolayout',
    },
  },
  {
    path: '/profile',
    name: 'Profile',
    meta: {
      layout: 'deflayout',
      requiresAuth: true,
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "profile" */ '../views/Profile.vue'),
  },
  {
    path: '*',
    name: 'NotFound',
    meta: {
      layout: 'nolayout',
      requiresAuth: false,
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "profile" */ '../views/404.vue'),
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  
  const isLoggedIn = isUserLoggedIn()

  if(isLoggedIn===false){
    store.commit('logUserOut')
  }

  const configuration =Object.assign({}, {
    path: to.path,
    params: { nextUrl: to.fullPath },
    query: { lang: from.query.lang || 'fa' }
  });

  if (to.name === 'Login' && isLoggedIn) {
    next(Object.assign({}, configuration, { path: '/' }));
  }
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!isLoggedIn) {
      next(Object.assign({}, configuration, { path: '/login', }));
    } else {
      to.query.lang ? next() : next(configuration)
    }
  } else {
    to.query.lang ? next() : next(configuration)
  }
});

export default router;

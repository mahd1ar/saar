import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#4582ec",
        secondary: "#adb5bd",
        accent: "#82B1FF",
        error: "#d9534f",
        info: "#17a2b8",
        success: "#02b875",
        warning: "#f0ad4e"
      },
      dark: {
        primary: "#375a7f",
        secondary: "#444",
        accent: "#222",
        error: "#e74c3c",
        info: "#3498db",
        success: "#00bc8c",
        warning: "#f39c12"
      }
    }
  }
});

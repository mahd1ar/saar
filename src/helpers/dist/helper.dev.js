"use strict";

var _interopRequireDefault = require("/home/mahdiyar/Documents/proj/saar/node_modules/@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/es.object.to-string");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAverageRGB = getAverageRGB;
exports.timeout = timeout;
exports.isUserLoggedIn = isUserLoggedIn;
exports.fillStrorage = fillStrorage;
exports.expAt = expAt;
exports.clearStorage = clearStorage;
exports.mergeRouteQuery = mergeRouteQuery;

var _lodash = _interopRequireDefault(require("lodash.clonedeep"));

/* eslint-disable */
function getAverageRGB(imgEl) {
  var blockSize = 5; // only visit every 5 pixels

  var defaultRGB = {
    r: 0,
    g: 0,
    b: 0
  }; // for non-supporting envs

  var canvas = document.createElement('canvas');
  var context = canvas.getContext && canvas.getContext('2d');
  var data;
  var width;
  var height;
  var i = -4;
  var length;
  var rgb = {
    r: 0,
    g: 0,
    b: 0
  };
  var count = 0;

  if (!context) {
    return defaultRGB;
  }

  height = canvas.height = imgEl.naturalHeight || imgEl.offsetHeight || imgEl.height;
  width = canvas.width = imgEl.naturalWidth || imgEl.offsetWidth || imgEl.width;
  context.drawImage(imgEl, 0, 0);

  try {
    data = context.getImageData(0, 0, width, height);
  } catch (e) {
    /* security error, img on diff domain */
    return defaultRGB;
  }

  length = data.data.length;

  while ((i += blockSize * 4) < length) {
    ++count;
    rgb.r += data.data[i];
    rgb.g += data.data[i + 1];
    rgb.b += data.data[i + 2];
  } // ~~ used to floor values


  rgb.r = ~~(rgb.r / count);
  rgb.g = ~~(rgb.g / count);
  rgb.b = ~~(rgb.b / count);
  return rgb;
}

function timeout(time) {
  return new Promise(function (resolve) {
    return setTimeout(function () {
      return resolve();
    }, time);
  });
}

function isUserLoggedIn() {
  var currentUnixTimestap = ~~(+new Date() / 1000);
  var expires = localStorage.getItem('exp');
  return expires ? expires > currentUnixTimestap : false;
}

function expAt(epoch) {
  localStorage.setItem('exp', epoch);
}

function clearStorage() {
  localStorage.removeItem('authTocken');
  localStorage.removeItem('username');
  localStorage.removeItem('exp');
}

function fillStrorage(params) {
  var token = params.token,
      id = params.id,
      username = params.username,
      email = params.email,
      fristName = params.fristName,
      lastName = params.lastName;
  localStorage.setItem('authTocken', token);
  localStorage.setItem('username', username);
}

function mergeRouteQuery(route, customQuery) {
  var forceToReplace = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  var r = (0, _lodash.default)(route);
  var curr_q = r.query || {};
  var cust_q = (0, _lodash.default)(customQuery);
  var res_q = forceToReplace ? Object.assign(curr_q, cust_q) : Object.assign(cust_q, curr_q);
  r.query = res_q;
  return r;
}
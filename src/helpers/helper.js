import cloneDeep from "lodash.clonedeep";

/* eslint-disable */
function getAverageRGB(imgEl) {
  const blockSize = 5; // only visit every 5 pixels
  const defaultRGB = { r: 0, g: 0, b: 0 }; // for non-supporting envs
  const canvas = document.createElement('canvas');
  const context = canvas.getContext && canvas.getContext('2d');
  let data; let width; let height;
  let i = -4;
  let length;
  const rgb = { r: 0, g: 0, b: 0 };
  let count = 0;

  if (!context) {
    return defaultRGB;
  }

  height = canvas.height = imgEl.naturalHeight || imgEl.offsetHeight || imgEl.height;
  width = canvas.width = imgEl.naturalWidth || imgEl.offsetWidth || imgEl.width;

  context.drawImage(imgEl, 0, 0);

  try {
    data = context.getImageData(0, 0, width, height);
  } catch (e) {
    /* security error, img on diff domain */
    return defaultRGB;
  }

  length = data.data.length;

  while ((i += blockSize * 4) < length) {
    ++count;
    rgb.r += data.data[i];
    rgb.g += data.data[i + 1];
    rgb.b += data.data[i + 2];
  }

  // ~~ used to floor values
  rgb.r = ~~(rgb.r / count);
  rgb.g = ~~(rgb.g / count);
  rgb.b = ~~(rgb.b / count);

  return rgb;
}

function timeout(time) {
  return new Promise(resolve => setTimeout(() => resolve(), time))
}

function isUserLoggedIn() {
  const currentUnixTimestap = ~~(+new Date() / 1000)
  const expires = localStorage.getItem('exp');
  return expires ? expires > currentUnixTimestap : false;
}

function expAt(epoch) {
  localStorage.setItem('exp', epoch)
}

function clearStorage() {
  localStorage.removeItem('authTocken');
  localStorage.removeItem('username');
  localStorage.removeItem('exp')
}

function fillStrorage(params) {
  const { token, id, username, email, fristName, lastName } = params
  localStorage.setItem('authTocken', token);
  localStorage.setItem('username', username);
}

function mergeRouteQuery(route, customQuery, forceToReplace = false) {
  const r = cloneDeep(route);
  const curr_q = r.query || {};
  const cust_q = cloneDeep(customQuery);
  const res_q = forceToReplace ? Object.assign(curr_q, cust_q) : Object.assign(cust_q, curr_q);
  r.query = res_q;
  return r;
}

export {
  getAverageRGB,
  timeout,
  isUserLoggedIn,
  fillStrorage,
  expAt,
  clearStorage,
  mergeRouteQuery
};

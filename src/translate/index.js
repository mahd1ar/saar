const EN = {};
const FA = {};

function arr2obj(obj, parent = null) {
  // eslint-disable-next-line
  for (let j = 0 ;j< Object.keys(obj).length;j++) {
    const i = Object.keys(obj)[j];
    if (Array.isArray(obj[i])) {
      const en = obj[i][0] === "-" ? i.replace(/([A-Z])/g, " $1").toLowerCase() : obj[i][0];
      // const en = obj[i][0];
      const fa = obj[i][1];

      EN[parent][i] = en;
      FA[parent][i] = fa;
    } else if (typeof obj === "object") {
      EN[i] = {};
      FA[i] = {};

      arr2obj(obj[i], i);
    }
  }
}

const messages = {
  application: {
    saar: ["Saar", "ثار"],
    slogan: ["Idea and event registration system", "سامانه ثبت ایده و رخداد"]
  },
  general: {
    err: ["error", "خطا"],
    clear: ["-", "پاکسازی"],
    submit: ["-", "ثبت"],
    save: ["-", "ذخیره"]
  },
  snackbar: {
    Unauthorized: ["-", "غیر مجاز"],
    Forbidden: ["-", "دسترسی ممنوع"],
    NotFound: ["-", "پیدا نشد"],
    NetworkError: ["-", "خطا در برقراری ارتباط با شبکه"],
    serverError: ["-", "خطا از طرف سرور رخ داده"],
    unknownError: [
      "unknown error: check console to learn more",
      "خطای نا معلوم : برای اطلاعات بیشتر کنسول خود را چک کنید"
    ],
    seeYouSoon: ["-", "به امید دیدار"]
  },
  profile: {
    profile: ["-", "پروفایل"],
    title: ["‍‍profile", "‍‍پروفایل"],
    firstName: ["-", "نام"],
    lastName: ["-", "نام خانوادگی"],
    email: ["-", "ایمیل"],
    password: ["-", "پسورد"],
    confirmPassword: ["-", "تایید پسورد"],
    phoneNumber: ["-", "شماره تماس"],
    ssn: ["social security number", "کد ملی"],
    userName: ["-", "نام کاربری"]
  },
  signup: {
    signUp: ["-", "ساخت حساب کاربری"],
    signUpBtn: ["create new account", "ساخت حساب جدید"]
  },
  login: {
    login: ["-", "ورود"],
    loginCapture: ["login to see more", "برای ادامه با نام کاربری خود لاگین کنید"],
    sessionExpired: ["your session has expired", "جلسه باطل شده"],
    noAccount: ["dont have an account ? signup now", "ایجاد حساب کاربری"],
    userName: ["-", "نام کاربری"],
    password: ["-", "پسورد"]
  },
  dropDownMenu: {
    Profile: ["-", "پروفایل"],
    Dashboard: ["-", "داشبرد"],
    changeLanguage: ["-", "تغییر زبان"],
    currentLanguage: [" current language is English ", "زبان فعلی فارسی است"],
    LogOut: ["-", "خروج از سیستم"]
  },
  message: {
    welcome: ["Welcome!! 🎉", "🎉 !!خوش آمدید"]
  },
  home: {
    title: ["home", "خانه"],
    search: ["-", "جست و جو"]
  },
  registration: {
    title: ["new", "جدید"]
  },
  form: {
    title: ["-", "عنوان"],
    registration: ["registration form", "فرم ثبت محتوا"],
    firstAndLastName: ["-", "نام و نام خانوادگی"],
    email: ["-", "ایمیل"],
    status: ["-", "وضعیت"],
    description: ["-", "توصیف"],
    hashCode: ["-", "هش کد"],
    contentRegistrarName: ["-", " ثبت کننده محتوا"],
    contentOwnerName: ["-", "نام مالک محتوا"],
    contentFormat: ["-", "قالب محتوا"],
    submitTo: ["-", "تسلیم به"],
    tags: ["-", "تگ ها"]
  },
  hero: {
    welcome: ["welcome", "خوش آمدید"]
  },
  table: {
    date: ["-", "تاریخ"],
    name: ["title", "عنوان"],
    id: ["-", "شناسه"],
    email: ["-", "ایمیل"],
    format: ["-", "فرمت"],
    keywords: ["-", "تگ ها"],
    hash: ["-", "هش"],
    user: ["-", "کاربر"],
    details: ["-", "جزییات"],
    phone: ["-", "تلفن"],
    submitter: ["-", "ثبت کننده"],
    owner: ["-", "مالک"],
    docType: ["-", "نوع سند"],
    description: ["-", "توصیف"]
  },
  notFound: {
    404: ["404 NotFound", "صفحه مورد نظر پیدا نشد"]
  }
};

arr2obj(messages);

export default { en: EN, fa: FA };

import Vue from "vue";
import "./registerServiceWorker";
import VueI18n from "vue-i18n";
import CustomAlert from "@/components/CustomAlert.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
import store from "./store";
// components
import nolayout from "./layouts/nolayout.vue";
import deflayout from "./layouts/deflayout.vue";
// tranlation
import messages from "./translate";
// Vue application
import App from "./App.vue";

let vueApp;
const axios = require("axios");

axios.defaults.baseURL = process.env.VUE_APP_BASEURI;

axios.interceptors.response.use(
  response => {
    vueApp.$store.commit("waiting", false);
    return response;
  },
  error => {
    const { $store } = vueApp;
    vueApp.$store.commit("waiting", false);

    if (error.message === "Network Error") {
      $store.commit("snackbar", { msg: vueApp.$t("snackbar.NetworkError"), state: "error" });
    } else {
      switch (error.response.status) {
        case 401:
          $store.commit("snackbar", { msg: vueApp.$t("snackbar.Unauthorized"), state: "error" });
          $store.commit("logUserOut");
          break;
        case 403:
          $store.commit("snackbar", { msg: vueApp.$t("snackbar.Forbidden"), state: "error" });
          break;
        case 404:
          $store.commit("snackbar", { msg: vueApp.$t("snackbar.NotFound"), state: "error" });
          break;
        case 500:
          $store.commit("snackbar", { msg: vueApp.$t("snackbar.serverError"), state: "error" });
          break;
        default:
          $store.commit("snackbar", { msg: vueApp.$t("snackbar.unknownError"), state: "error" });
          console.log(error);
          break;
      }
    }

    // Do something with response error
    return Promise.reject(error);
  }
);

axios.interceptors.request.use(
  config => {
    vueApp.$store.commit("waiting", true);
    // Do something before request is sent

    // config.timeout = 65000
    const authTocken = localStorage.getItem("authTocken");

    // eslint-disable-next-line
  if (authTocken) config.headers.Authorization = 'Bearer  '+authTocken;

    return config;
  },
  error => Promise.reject(error)
); // Do something with request error

Vue.use(VueI18n);

Vue.prototype.$axios = axios;
Vue.component("nolayout", nolayout);
Vue.component("deflayout", deflayout);
Vue.component("customAlert", CustomAlert);

Vue.config.productionTip = false;

const i18n = new VueI18n({
  locale: "en", // set locale
  messages // set locale messages
});

vueApp = new Vue({
  router,
  vuetify,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
